# Clean Code Assignment - Thiago Santos

## Layers

- Model;
- Controller;
- UI.

## Low coupling and high cohesion

- Classes don't include what is out of their responsibility's scope;
- Controller receives adapters by DI.

## Design patterns

- Adapter;
- Controller.

## Design principles

- DRY - validateURL method from AssignmentController class - we could call validate() two times, inside handleCreate and handleUpdate;
- KISS and YAGNI - At the moment, the application only creates and updates assignments. It could have a delete feature, but it is not necessary to the Clean Code Assignment.

import { Model, ObjectId } from "mongoose";

export interface IAssignment {
  _id: ObjectId;
  name: string;
  link?: string;
}

export interface IAssignmentDocument
  extends Omit<IAssignment, "_id">,
    Document {}
export interface IAssignmentModel extends Model<IAssignmentDocument> {}

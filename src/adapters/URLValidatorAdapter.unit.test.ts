import { URLValidatorAdapter } from "./URLValidatorAdapter";
import faker from "faker";

describe("URLValidatorAdapter", () => {
  test("should return undefined if URL is valid", () => {
    const adapter = new URLValidatorAdapter();

    expect(adapter.validate(faker.internet.url())).toBe(undefined);
  });

  test("should return error if URL is valid", () => {
    const adapter = new URLValidatorAdapter();

    expect(adapter.validate(faker.random.alphaNumeric(5))).toEqual([
      "Website is not a valid url",
    ]);
  });
});

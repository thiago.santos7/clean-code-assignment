jest.useFakeTimers();
import { IAssignment, IAssignmentModel } from "../models/Assignment";
import { MongooseAdapter } from "./DatabaseAdapter";
import faker from "faker";

const makeSUT = () => {
  const save = jest.fn();
  const findOne = jest.fn().mockImplementation(() => ({
    exec: jest.fn(),
  }));
  const findOneAndUpdate = jest.fn().mockImplementation(() => ({
    exec: jest.fn(),
  }));

  const model = jest.fn().mockImplementation((_) => {
    return {
      save,
    };
  }) as unknown as IAssignmentModel;

  model.findOne = findOne;
  model.findOneAndUpdate = findOneAndUpdate;

  const adapter = new MongooseAdapter(model as unknown as IAssignmentModel);

  return {
    adapter,
    save,
    findOne,
    findOneAndUpdate,
  };
};

describe("DatabaseAdapter", () => {
  test("should create assignment on database", async () => {
    const { adapter, save } = makeSUT();

    await adapter.create({ name: "Teste" } as IAssignment);

    expect(save).toHaveBeenCalled();
  });

  test("should update assignment on database", async () => {
    const { adapter, findOneAndUpdate } = makeSUT();
    const name = faker.name.firstName();

    await adapter.update({ name } as IAssignment);

    expect(findOneAndUpdate).toHaveBeenCalledWith(
      { name: name },
      { name: name },
      { new: true }
    );
  });

  test("should find assignment on database", async () => {
    const { adapter, findOne } = makeSUT();
    const name = faker.name.firstName();

    await adapter.find(name);

    expect(findOne).toHaveBeenCalledWith({ name: name });
  });
});

import {
  IAssignment,
  IAssignmentDocument,
  IAssignmentModel,
} from "../models/Assignment";
import mongoose, { Schema } from "mongoose";

export interface IDatabaseAdapter {
  create: (assignment: IAssignment) => Promise<IAssignmentDocument>;
  update: (assignment: IAssignment) => Promise<IAssignmentDocument>;
  find: (name: string) => Promise<IAssignmentDocument | null>;
}

export class MongooseAdapter implements IDatabaseAdapter {
  private Assignment: IAssignmentModel;

  constructor(model: IAssignmentModel) {
    this.Assignment = model;
  }

  async create(assignment: IAssignment): Promise<IAssignmentDocument> {
    try {
      const newAssignment = new this.Assignment(assignment);

      let savedAssignment = await newAssignment.save();

      return savedAssignment;
    } catch (err) {
      throw err;
    }
  }

  async update(assignment: IAssignment): Promise<IAssignmentDocument> {
    try {
      return (await this.Assignment.findOneAndUpdate(
        { name: assignment.name },
        assignment,
        {
          new: true,
        }
      ).exec()) as IAssignmentDocument;
    } catch (err) {
      throw err;
    }
  }

  async find(name: string): Promise<IAssignmentDocument | null> {
    try {
      return await this.Assignment.findOne({
        name,
      }).exec();
    } catch (err) {
      throw err;
    }
  }
}

mongoose.connect("mongodb://localhost:27017/test", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

const assignment = { name: String, link: String };
const schema = new mongoose.Schema(assignment) as Schema<IAssignmentDocument>;
const model = mongoose.connection.model("Assignment", schema);

const adapter = new MongooseAdapter(model);

export default adapter;

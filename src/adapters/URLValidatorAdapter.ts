import validator from "validate.js";

export interface IURLValidator {
  validate: (url: string) => undefined | string;
}

export class URLValidatorAdapter implements IURLValidator {
  validate(url: string) {
    return validator({ website: url }, { website: { url: true } })?.website;
  }
}

const adapter = new URLValidatorAdapter();

export default adapter;

import { Router } from "express";

import databaseAdapter from "../adapters/DatabaseAdapter";
import validatorAdapter from "../adapters/URLValidatorAdapter";
import AssignmentController from "../controllers/AssignmentController";

const router = Router();

const assignmentController = new AssignmentController(
  databaseAdapter,
  validatorAdapter
);

router.post("/save-assignment", (req, res) =>
  assignmentController.handleCreate(req, res)
);

router.post("/update-assignment", (req, res) =>
  assignmentController.handleUpdate(req, res)
);

export { router };

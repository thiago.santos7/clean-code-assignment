import request from "supertest";
import faker from "faker";

import app from "./app";

describe("Operation endpoint", () => {
  const name = faker.name.firstName();

  test("should save assignment", async () => {
    const res = await request(app)
      .post("/save-assignment")
      .send({ name })
      .expect(200);

    expect(res.body).toHaveProperty("_id");
  });

  test("should update assignment", async () => {
    const link = faker.internet.url();

    const res = await request(app)
      .post("/update-assignment")
      .send({
        name,
        link,
      })
      .expect(200);

    expect(res.body).toHaveProperty("link", link);
  });
});

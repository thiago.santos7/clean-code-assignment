import { Request, Response } from "express";
import AssignmentController from "./AssignmentController";
import faker from "faker";

const makeSUT = () => {
  const create = jest.fn().mockResolvedValue((obj: any) => obj);
  const find = jest.fn().mockResolvedValue(null);
  const update = jest.fn().mockResolvedValue((obj: any) => obj);

  const databaseAdapter = { find, create, update };

  const validate = jest.fn();
  const urlValidatorAdapter = { validate };

  const controller = new AssignmentController(
    databaseAdapter,
    urlValidatorAdapter
  );

  return { controller, databaseAdapter, urlValidatorAdapter };
};

describe("AssignmentController", () => {
  test("should handle creation", async () => {
    const { controller, databaseAdapter } = makeSUT();

    await controller.handleCreate(
      { body: { name: faker.name.firstName() } } as Request,
      {
        status: (code: number) => {
          return { json: (obj: any) => obj };
        },
      } as unknown as Response
    );

    expect(databaseAdapter.create).toHaveBeenCalled();
  });

  test("should handle update", async () => {
    const { controller, databaseAdapter } = makeSUT();

    await controller.handleUpdate(
      { body: { name: faker.name.firstName() } } as Request,
      {
        status: (code: number) => {
          return { json: (obj: any) => obj };
        },
      } as unknown as Response
    );

    expect(databaseAdapter.update).toHaveBeenCalled();
  });

  test("should validate URL before handling creation", async () => {
    const { controller, urlValidatorAdapter } = makeSUT();

    await controller.handleUpdate(
      {
        body: { name: faker.name.firstName(), link: faker.internet.url() },
      } as Request,
      {
        status: (code: number) => {
          return { json: (obj: any) => obj };
        },
      } as unknown as Response
    );

    expect(urlValidatorAdapter.validate).toHaveBeenCalled();
  });

  test("should validate URL before handling update", async () => {
    const { controller, urlValidatorAdapter } = makeSUT();

    await controller.handleUpdate(
      {
        body: { name: faker.name.firstName(), link: faker.internet.url() },
      } as Request,
      {
        status: (code: number) => {
          return { json: (obj: any) => obj };
        },
      } as unknown as Response
    );

    expect(urlValidatorAdapter.validate).toHaveBeenCalled();
  });

  test("should validate name before handling creation", async () => {
    const { controller } = makeSUT();

    try {
      await controller.handleCreate(
        { body: { link: faker.internet.url() } } as Request,
        {
          status: (code: number) => {
            return { json: (obj: any) => obj };
          },
        } as unknown as Response
      );
    } catch (e) {
      expect(e.message).toBe("The property 'name' cannot be empty.");
    }
  });

  test("should validate name before handling update", async () => {
    const { controller } = makeSUT();

    try {
      await controller.handleUpdate(
        { body: { link: faker.internet.url() } } as Request,
        {
          status: (code: number) => {
            return { json: (obj: any) => obj };
          },
        } as unknown as Response
      );
    } catch (e) {
      expect(e.message).toBe("The property 'name' cannot be empty.");
    }
  });
});

import { Request, Response } from "express";
import { IDatabaseAdapter } from "../adapters/DatabaseAdapter";
import { IURLValidator } from "../adapters/URLValidatorAdapter";
import { IAssignment } from "../models/Assignment";

export default class AssignmentController {
  private databaseAdapter: IDatabaseAdapter;
  private urlValidatorAdapter: IURLValidator;

  constructor(
    databaseAdapter: IDatabaseAdapter,
    urlValidatorAdapter: IURLValidator
  ) {
    this.databaseAdapter = databaseAdapter;
    this.urlValidatorAdapter = urlValidatorAdapter;
  }

  private validateName(name: string) {
    if (!name) throw new Error("The property 'name' cannot be empty.");
  }

  private validateURL(url: string) {
    const validationResult = this.urlValidatorAdapter.validate(url);

    if (validationResult) throw new Error(validationResult);
  }

  async handleCreate(req: Request, res: Response) {
    const assignment = req.body as unknown as IAssignment;

    try {
      if (assignment?.link) this.validateURL(assignment.link);

      this.validateName(assignment?.name);

      const findResult = await this.databaseAdapter.find(assignment.name);

      const createdAssignment =
        findResult ?? (await this.databaseAdapter.create(assignment));
      return res.status(200).json(createdAssignment);
    } catch (e) {
      return res.status(400).json({ error: e.message });
    }
  }

  async handleUpdate(req: Request, res: Response) {
    const assignment = req.body as unknown as IAssignment;

    try {
      if (assignment?.link) this.validateURL(assignment.link);

      this.validateName(assignment?.name);

      const updatedAssignment = await this.databaseAdapter.update(assignment);

      if (!updatedAssignment) {
        return res
          .status(404)
          .json({ error: "No user was found for the provided name." });
      }

      return res.status(200).json(updatedAssignment);
    } catch (e) {
      return res.status(400).json({ error: e.message });
    }
  }
}
